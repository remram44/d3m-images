FROM registry.gitlab.com/datadrivendiscovery/images/libs:ubuntu-bionic-python38

RUN \
 pip3 install -e git+https://gitlab.com/datadrivendiscovery/d3m.git@devel#egg=d3m && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes --no-install-recommends patch && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

COPY ./patches /patches

RUN \
 for patch in /patches/*; do patch --directory=/usr/local/lib/python3.8/ --prefix=/patches/ -p0 --force "--input=/$patch" || exit 1; done && \
 rm -rf /patches

ARG org_datadrivendiscovery_public_source_commit
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_SOURCE_COMMIT=$org_datadrivendiscovery_public_source_commit

ARG org_datadrivendiscovery_public_base_digest
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_BASE_DIGEST=$org_datadrivendiscovery_public_base_digest
