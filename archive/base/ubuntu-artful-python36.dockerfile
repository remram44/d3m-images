FROM ubuntu:artful

ENV DEBIAN_FRONTEND noninteractive

# apt-utils seems missing and warnings are shown, so we install it.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes apt-utils tzdata locales file sudo && \
 echo 'UTC' > /etc/timezone && \
 rm /etc/localtime && \
 dpkg-reconfigure tzdata && \
 apt-get upgrade --yes --force-yes && \
 rm -f /etc/cron.weekly/fstrim && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Prevent Python packages to be installed through APT.
COPY ./etc/apt/preferences.d/no-python2 /etc/apt/preferences.d/no-python2

# General dependencies for building dependencies, locales, and some utilities.
RUN sed -i 's/^# deb-src/deb-src/' /etc/apt/sources.list && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes --no-install-recommends swig git build-essential cmake wget ssh tar gzip ca-certificates unzip curl libcurl4-openssl-dev libssl-dev equivs vim-tiny && \
 locale-gen --no-purge en_US.UTF-8 && \
 update-locale LANG=en_US.UTF-8 && \
 echo locales locales/locales_to_be_generated multiselect en_US.UTF-8 UTF-8 | debconf-set-selections && \
 echo locales locales/default_environment_locale select en_US.UTF-8 | debconf-set-selections && \
 dpkg-reconfigure locales && \
 update-alternatives --install /usr/bin/vim vim /usr/bin/vim.tiny 10 && \
 curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash && \
 apt-get install --yes --force-yes git-lfs && \
 git lfs install && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Docker in Docker.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes apt-transport-https ca-certificates && \
 curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - && \
 echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable" > /etc/apt/sources.list.d/docker.list && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes docker-ce && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Set locale to UTF-8 which makes Python read text in UTF-8 and not ASCII, by default.
ENV LC_ALL=en_US.UTF-8

# Installing Python 3.6.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes python3.6 python3.6-dev python3-pip python3-openssl && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Base Python packages (those dependencies from d3m core package which are needed also in testing image).
RUN pip3 install docker[tls]==2.7 && \
 pip3 install --upgrade setuptools && \
 pip3 install frozendict==1.2 && \
 pip3 install PyYAML==3.12 && \
 pip3 install pycurl==7.43.0.1 && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Dependencies needed to build Python itself.
RUN apt-get update -q -q && \
 apt-get build-dep --yes --force-yes python3.6 && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Extra Python dependencies.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes --no-install-recommends zlib1g-dev libexpat1-dev tk8.6-dev libffi-dev libssl-dev \
 libbz2-dev liblzma-dev libncurses5-dev libreadline6-dev libsqlite3-dev libgdbm-dev liblzma-dev lzma lzma-dev && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Dummy package to satisfy unnecessary dependency on Python 2.
# See: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=891712
COPY ./equivs/python-dev /tmp/python-dev
RUN cd /tmp && \
 equivs-build python-dev && \
 dpkg -i python-dev-dummy_1.0_all.deb && \
 cd / && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm
