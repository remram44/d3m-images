FROM registry.gitlab.com/datadrivendiscovery/images/base:ubuntu-bionic-python36

ENV D3M_INTERFACE_VERSION=v2019.6.7

RUN \
 pip3 install git+https://gitlab.com/datadrivendiscovery/d3m.git@$D3M_INTERFACE_VERSION#egg=d3m && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

ARG org_datadrivendiscovery_public_source_commit
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_SOURCE_COMMIT=$org_datadrivendiscovery_public_source_commit

ARG org_datadrivendiscovery_public_base_digest
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_BASE_DIGEST=$org_datadrivendiscovery_public_base_digest
