FROM registry.gitlab.com/datadrivendiscovery/images/base:ubuntu-xenial-python36

ENV D3M_INTERFACE_VERSION=v2018.1.26

RUN \
 pip3 install --process-dependency-links git+https://gitlab.com/datadrivendiscovery/metadata.git@$D3M_INTERFACE_VERSION && \
 pip3 install --process-dependency-links git+https://gitlab.com/datadrivendiscovery/primitive-interfaces.git@$D3M_INTERFACE_VERSION && \
 pip3 install --process-dependency-links git+https://gitlab.com/datadrivendiscovery/d3m.git@$D3M_INTERFACE_VERSION && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm
