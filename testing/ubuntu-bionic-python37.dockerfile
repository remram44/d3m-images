FROM registry.gitlab.com/datadrivendiscovery/images/shared:ubuntu-bionic-python37

VOLUME /var/lib/docker

# Docker in Docker.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes apt-transport-https ca-certificates && \
 curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - && \
 echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable" > /etc/apt/sources.list.d/docker.list && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes docker-ce && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Python.
RUN \
 apt-get update -q -q && \
 apt-get install --yes --force-yes build-essential libcap-dev ffmpeg jq curl libcurl4-openssl-dev && \
 pip3 install python-prctl==1.7 && \
 pip3 install pycodestyle==2.4.0 && \
 pip3 install git+https://github.com/python/mypy.git@8577ea4c0cfbc9b3ccfcc74b670bd40089a783a8 && \
 pip3 install deepdiff==3.3.0 && \
 pip3 install pyquery==1.4.0 && \
 pip3 install yattag==1.10.0 && \
 pip3 install deep_dircmp==0.1.0 && \
 pip3 install recommonmark==0.5.0 && \
 pip3 install sphinx==3.5.4 && \
 pip3 install sphinxcontrib-fulltoc==1.2.0 && \
 pip3 install git+https://github.com/david-yan/sphinx-autodoc-typehints.git@a9ed7e9257ae79759c9e77478d626c518881a164 && \
 pip3 install virtualenv==16.2.0 && \
 pip3 install asv==0.4.2 && \
 pip3 install yq==2.9.2 && \
 pip3 install grpcio grpcio-tools && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Go.
ENV GOBIN=/usr/local/go/bin
ENV PATH="${GOBIN}:${PATH}"
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes git curl unzip wget && \
 curl -L https://golang.org/dl/go1.16.5.linux-amd64.tar.gz | tar -C /usr/local -xz && \
 go get -u github.com/golang/protobuf/proto && \
 go get -u google.golang.org/grpc && \
 go install github.com/golang/protobuf/protoc-gen-go@v1.5.2 && \
 go install github.com/ckaznocha/protoc-gen-lint@v0.2.3 && \
 curl -OL https://github.com/google/protobuf/releases/download/v3.6.1/protoc-3.6.1-linux-x86_64.zip && \
 unzip protoc-3.6.1-linux-x86_64.zip -d protoc3 && \
 cp -a protoc3/bin/protoc /usr/bin/protoc && \
 mkdir -p /usr/local/include/google && \
 cp -a protoc3/include/google/protobuf /usr/local/include/google && \
 rm -rf protoc-3.6.1-linux-x86_64.zip && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# JavaScript.
RUN wget -O - https://nodejs.org/dist/v10.15.1/node-v10.15.1-linux-x64.tar.xz | tar Jx --strip=1 -C /usr/local --anchored --exclude=node-v10.15.1-linux-x64/CHANGELOG.md --exclude=node-v10.15.1-linux-x64/LICENSE --exclude=node-v10.15.1-linux-x64/README.md && \
 npm install grpc && \
 npm install google-protobuf && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# NVIDIA Container Toolkit.
RUN \
 curl -fsSL https://nvidia.github.io/nvidia-docker/gpgkey | apt-key add - && \
 curl -fsSL "https://nvidia.github.io/nvidia-docker/$(. /etc/os-release; echo $ID$VERSION_ID)/nvidia-docker.list" > /etc/apt/sources.list.d/nvidia-docker.list && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes nvidia-container-toolkit && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Kubernetes in Docker.
RUN \
 mkdir -p /usr/local/go/src/sigs.k8s.io && \
 cd /usr/local/go/src/sigs.k8s.io && \
 git clone https://github.com/kubernetes-sigs/kind && cd kind && git checkout 161151a26faf0dbe962ac9f323cc0cdebac79ba8 && go install . && \
 curl -fsSL https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - && \
 echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" > /etc/apt/sources.list.d/kubernetes.list && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes kubectl && \
 pip3 install git+https://github.com/mitar/kubernetes-python.git && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

ARG org_datadrivendiscovery_public_source_commit
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_SOURCE_COMMIT=$org_datadrivendiscovery_public_source_commit

ARG org_datadrivendiscovery_public_base_digest
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_BASE_DIGEST=$org_datadrivendiscovery_public_base_digest
