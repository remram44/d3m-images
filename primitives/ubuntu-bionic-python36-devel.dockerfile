FROM registry.gitlab.com/datadrivendiscovery/images/core:ubuntu-bionic-python36-devel

# This assumes "build.sh" cloned the repository.
COPY ./primitives /primitives
COPY ./install-primitives.py /install-primitives.py

# After installing all primitives, we check that the image has consistent dependencies
# using "pip3 check". The goal here is that image building fails if this is not true.
# We also check that the devel version of the core package is still installed and
# that it has not been changed to something else. Furthermore, we patch information
# about dependencies on the core package, for "pip3 check" to not complain about that.
# We remove all /src/*/build directories as a workaround for the pip bug.
# See: https://github.com/pypa/pip/issues/6521
RUN cd / && \
 python3 /install-primitives.py --annotations primitives/primitives && \
 rm -rf /primitives /install-primitives.py && \
 pip3 --disable-pip-version-check show d3m && \
 pip3 --disable-pip-version-check show d3m | grep -E '^Version:.*\.dev0$' > /dev/null && \
 find /src /usr/local/lib/python3.6/dist-packages/ -name requires.txt -print0 | xargs -r -0 sed -i -E 's/^d3m[<>=]{2}.*$/d3m/g' && \
 find /src /usr/local/lib/python3.6/dist-packages/ -path '*dist-info/METADATA' -print0 | xargs -r -0 sed -i -E 's/^Requires-Dist: d3m .*$/Requires-Dist: d3m/g' && \
 pip3 --disable-pip-version-check check && \
 rm -rf /src/*/build && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

ARG org_datadrivendiscovery_public_source_commit
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_SOURCE_COMMIT=$org_datadrivendiscovery_public_source_commit

ARG org_datadrivendiscovery_public_base_digest
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_BASE_DIGEST=$org_datadrivendiscovery_public_base_digest

ARG org_datadrivendiscovery_public_primitives_commit
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_PRIMITIVES_COMMIT=$org_datadrivendiscovery_public_primitives_commit

ARG org_datadrivendiscovery_public_timestamp
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_TIMESTAMP=$org_datadrivendiscovery_public_timestamp
